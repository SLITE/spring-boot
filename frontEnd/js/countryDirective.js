angular.module('countryDirective', [])
       .directive('country', function(){
  return {
    /*scope: { country: '=' },*/
    restrict: 'A',
    templateUrl: 'template/country.html'
    controller: function($scope, countries){
      $scope.data ='main122';
      //$scope.msg = $scope.countries.msg;
      countries.find($scope.country.id, function(country) {
        $scope.flagURL = country.flagURL;
      });
    }
  };
});