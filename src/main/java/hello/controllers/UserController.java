package hello.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import hello.models.Task;
import hello.models.TaskDAO;
import hello.models.User;
import hello.models.UserDAO;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.fasterxml.jackson.databind.ObjectMapper;


import java.util.*;

/**
 * Created by dmsh0714 on 06.07.2016.
 */
@Controller
public class UserController {
    @Autowired
    private UserDAO userDao;

    @Autowired
    private TaskDAO taskDao;

    @RequestMapping("/create")
    @ResponseBody
    public String create(String name,Integer salary) {
        User user = null;
        try {
            user = new User(name, salary);
            userDao.save(user);
        }
        catch (Exception ex) {
            return "Error creating the user: " + ex.toString();
        }
        return "User succesfully created! (id = " + user.getId() + ")";
    }

    @RequestMapping("/get-by-name")
    @ResponseBody
    public String getByName(String name) {
        String userId;
        String salary;
        try {
            User user = userDao.findByName(name);
            userId = String.valueOf(user.getId());
            salary =  String.valueOf(user.getSalary());
        }
        catch (Exception ex) {
            return "User not found";
        }
        return "The user id is: " + userId+" salary -"+salary;
    }

    @RequestMapping("/get-task")
    @ResponseBody
    @PreAuthorize("hasRole('ROLE_USER')")
    public String getTask(long id) throws JsonProcessingException {

        Set<Task> tasks;
        try {
            User user = userDao.findOne(id);
            tasks = user.getTasks();
        }
        catch (Exception ex) {
            return "User not found";
        }
        String res = "Empty";

        ObjectMapper mapper = new ObjectMapper();
        try {
           res += mapper.writeValueAsString(tasks.iterator().next());
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return mapper.writeValueAsString(tasks);
    }

    @RequestMapping("/get-pass")
    @ResponseBody
    @PreAuthorize("hasRole('ROLE_USER')")
    public String getPass(){
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

        String hashedPassword = passwordEncoder.encode("123456");
        boolean isTrue =passwordEncoder.matches("123456",hashedPassword);
        return String.valueOf(isTrue);
    }

    @RequestMapping("/delete")
    @ResponseBody
    public String delete(long id) {
        try {
            User user = new User(id);
            userDao.delete(user);
        }
        catch (Exception ex) {
            return "Error deleting the user: " + ex.toString();
        }
        return "User succesfully deleted!";
    }
}
