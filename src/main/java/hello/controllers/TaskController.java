package hello.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import hello.models.Task;
import hello.models.TaskDAO;
import hello.models.User;
import hello.models.UserDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by dmsh0714 on 06.07.2016.
 */
@RestController
public class TaskController {
    @Autowired
    private TaskDAO taskDao;

    @Autowired
    private UserDAO userDao;

    @RequestMapping("/createTask")
    @ResponseBody
    public String create(String title,Boolean status,long id) {
        User current_user;
        Task task = null;
        try {
            current_user = userDao.findOne(id);
            task = new Task(title, status,current_user);
            taskDao.save(task);
        }
        catch (Exception ex) {
            return "Error creating the user: " + ex.toString();
        }
        return "User succesfully created! (descr ="  + task.getDescription() + ")"+title+" status "+status+" id "+id;
    }

    @RequestMapping("/auth")
    @PreAuthorize("true")
    public String login(String name, String pass) throws JsonProcessingException {
        String res = "Failed";
        SecurityContextHolder.getContext().setAuthentication(
                new UsernamePasswordAuthenticationToken("user", "1234", AuthorityUtils.createAuthorityList("ROLE_USER")));
        return res;
    }
    @RequestMapping("/out")
    @PreAuthorize("true")
    public String out(String name) throws JsonProcessingException {
        SecurityContextHolder.clearContext();
        return "out";
    }

    @RequestMapping("/get-task-id")
    @PreAuthorize("hasRole('ROLE_USER')")
    public String getById(long id) throws JsonProcessingException {
        Task task;
        try {
            task = taskDao.findOne(id);
        }
        catch (Exception ex) {
            return "User not found";
        }       ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(task)+" Hello "+SecurityContextHolder.getContext().getAuthentication().getName();
    }
}
