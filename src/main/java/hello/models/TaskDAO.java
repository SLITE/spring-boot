package hello.models;

import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;

/**
 * Created by dmsh0714 on 06.07.2016.
 */
@Transactional
public interface TaskDAO extends CrudRepository<Task, Long> {
    public Task findByStatus(Boolean status);

    public Task findByUser(User user);
}
