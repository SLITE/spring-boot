package hello.models;

import javax.transaction.Transactional;

import org.springframework.data.repository.CrudRepository;

/**
 * Created by dmsh0714 on 06.07.2016.
 */
@Transactional
public interface UserDAO extends CrudRepository<User, Long> {
    public User findByName(String name);

    public User findBySalary(Integer salary);

    public User findByPosition(String position);

    public User findByLogin(String login);
}
